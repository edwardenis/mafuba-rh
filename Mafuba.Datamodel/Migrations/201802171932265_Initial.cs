namespace Mafuba.Datamodel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Candidates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Cedula = c.String(),
                        Name = c.String(),
                        AspirationJobId = c.Int(nullable: false),
                        AspirationSalary = c.Double(nullable: false),
                        RecommendedBy = c.String(),
                        State = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Jobs", t => t.AspirationJobId, cascadeDelete: true)
                .Index(t => t.AspirationJobId);
            
            CreateTable(
                "dbo.Jobs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Department = c.String(),
                        RiskLevel = c.Int(nullable: false),
                        LowestSalary = c.Double(nullable: false),
                        MaxSalary = c.Double(nullable: false),
                        State = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Cedula = c.String(),
                        Nombre = c.String(),
                        EntryDate = c.DateTime(nullable: false),
                        Department = c.String(),
                        JobId = c.Int(nullable: false),
                        Salary = c.Double(nullable: false),
                        CandidateId = c.Int(nullable: false),
                        State = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Jobs", t => t.JobId)
                .ForeignKey("dbo.Candidates", t => t.CandidateId)
                .Index(t => t.JobId)
                .Index(t => t.CandidateId);
            
            CreateTable(
                "dbo.Competences",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        State = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Trainings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        Level = c.Int(nullable: false),
                        Since = c.DateTime(nullable: false),
                        To = c.DateTime(nullable: false),
                        Institute = c.String(),
                        State = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.WorkExperiences",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Company = c.String(),
                        PositionOcuppied = c.String(),
                        Since = c.DateTime(nullable: false),
                        To = c.DateTime(nullable: false),
                        Salary = c.Double(nullable: false),
                        State = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Lenguages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        State = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirtsName = c.String(),
                        LastName = c.String(),
                        Username = c.String(),
                        Password = c.String(),
                        Role = c.Int(nullable: false),
                        LastAccess = c.DateTime(nullable: false),
                        State = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CompetenceCandidates",
                c => new
                    {
                        Competence_Id = c.Int(nullable: false),
                        Candidate_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Competence_Id, t.Candidate_Id })
                .ForeignKey("dbo.Competences", t => t.Competence_Id, cascadeDelete: true)
                .ForeignKey("dbo.Candidates", t => t.Candidate_Id, cascadeDelete: true)
                .Index(t => t.Competence_Id)
                .Index(t => t.Candidate_Id);
            
            CreateTable(
                "dbo.TrainingCandidates",
                c => new
                    {
                        Training_Id = c.Int(nullable: false),
                        Candidate_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Training_Id, t.Candidate_Id })
                .ForeignKey("dbo.Trainings", t => t.Training_Id, cascadeDelete: true)
                .ForeignKey("dbo.Candidates", t => t.Candidate_Id, cascadeDelete: true)
                .Index(t => t.Training_Id)
                .Index(t => t.Candidate_Id);
            
            CreateTable(
                "dbo.WorkExperienceCandidates",
                c => new
                    {
                        WorkExperience_Id = c.Int(nullable: false),
                        Candidate_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.WorkExperience_Id, t.Candidate_Id })
                .ForeignKey("dbo.WorkExperiences", t => t.WorkExperience_Id, cascadeDelete: true)
                .ForeignKey("dbo.Candidates", t => t.Candidate_Id, cascadeDelete: true)
                .Index(t => t.WorkExperience_Id)
                .Index(t => t.Candidate_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WorkExperienceCandidates", "Candidate_Id", "dbo.Candidates");
            DropForeignKey("dbo.WorkExperienceCandidates", "WorkExperience_Id", "dbo.WorkExperiences");
            DropForeignKey("dbo.TrainingCandidates", "Candidate_Id", "dbo.Candidates");
            DropForeignKey("dbo.TrainingCandidates", "Training_Id", "dbo.Trainings");
            DropForeignKey("dbo.CompetenceCandidates", "Candidate_Id", "dbo.Candidates");
            DropForeignKey("dbo.CompetenceCandidates", "Competence_Id", "dbo.Competences");
            DropForeignKey("dbo.Employees", "CandidateId", "dbo.Candidates");
            DropForeignKey("dbo.Candidates", "AspirationJobId", "dbo.Jobs");
            DropForeignKey("dbo.Employees", "JobId", "dbo.Jobs");
            DropIndex("dbo.WorkExperienceCandidates", new[] { "Candidate_Id" });
            DropIndex("dbo.WorkExperienceCandidates", new[] { "WorkExperience_Id" });
            DropIndex("dbo.TrainingCandidates", new[] { "Candidate_Id" });
            DropIndex("dbo.TrainingCandidates", new[] { "Training_Id" });
            DropIndex("dbo.CompetenceCandidates", new[] { "Candidate_Id" });
            DropIndex("dbo.CompetenceCandidates", new[] { "Competence_Id" });
            DropIndex("dbo.Employees", new[] { "CandidateId" });
            DropIndex("dbo.Employees", new[] { "JobId" });
            DropIndex("dbo.Candidates", new[] { "AspirationJobId" });
            DropTable("dbo.WorkExperienceCandidates");
            DropTable("dbo.TrainingCandidates");
            DropTable("dbo.CompetenceCandidates");
            DropTable("dbo.Users");
            DropTable("dbo.Lenguages");
            DropTable("dbo.WorkExperiences");
            DropTable("dbo.Trainings");
            DropTable("dbo.Competences");
            DropTable("dbo.Employees");
            DropTable("dbo.Jobs");
            DropTable("dbo.Candidates");
        }
    }
}
