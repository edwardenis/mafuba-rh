﻿using Mafuba.Datamodel.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mafuba.Datamodel.Context
{
    public class MafubaContext : DbContext
    {
        public DbSet<Candidate> Candidates { get; set; }
        public DbSet<Competence> Competences { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Job> Jobs { get; set; }
        public DbSet<Training> Trainings { get; set; }
        public DbSet<WorkExperience> WorkExperiences { get; set; }
        public DbSet<Lenguage> Lenguages { get; set; }
        public DbSet<User> Users { get; set; }

        public MafubaContext() : base("DbConnection")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Candidate>()
                .HasMany(c => c.Employees)
                .WithRequired(e => e.Candidate)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Employee>()
                .HasRequired(e => e.Job)
                .WithMany(e => e.Employees)
                .WillCascadeOnDelete(false);
        }
    }
}
