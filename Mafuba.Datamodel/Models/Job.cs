﻿using Mafuba.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mafuba.Datamodel.Models
{
    public class Job : Basemodel
    {
        public string Name { get; set; }
        public string Department { get; set; }
        public RiskLevel RiskLevel { get; set; }
        public double LowestSalary { get; set; }
        public double MaxSalary { get; set; }

        public virtual ICollection<Employee> Employees { get; set; }
    }
}
