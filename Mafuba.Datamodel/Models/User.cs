﻿using Mafuba.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mafuba.Datamodel.Models
{
    public class User : Basemodel
    {
        public string FirtsName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public UserRoles Role { get; set; }
        public DateTime LastAccess { get; set; }

    }
}
