﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mafuba.Datamodel.Models
{
    public class WorkExperience : Basemodel
    {
        public string Company { get; set; }
        public string PositionOcuppied { get; set; }
        public DateTime Since { get; set; }
        public DateTime To { get; set; }
        public double Salary { get; set; }

        public virtual ICollection<Candidate> Candidates { get; set; }
    }
}
