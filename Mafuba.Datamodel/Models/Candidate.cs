﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mafuba.Datamodel.Models
{
    public class Candidate : Basemodel
    {
        public Candidate()
        {
            WorkExperiences = new List<WorkExperience>();
        }
        public string Cedula { get; set; }
        public string Name { get; set; }
        public int AspirationJobId { get; set; }
        public double AspirationSalary { get; set; }
        public string RecommendedBy { get; set; }
        [NotMapped]
        public string AspirationJobName { get { return AspirationJob?.Name; } }
        public virtual Job AspirationJob { get; set; }
        public virtual ICollection<Employee> Employees { get; set; }
        public virtual ICollection<Competence> PrincipalCompetencies { get; set; }
        public virtual ICollection<Training> PrincpialTrainings { get; set; }
        public virtual ICollection<WorkExperience> WorkExperiences { get; set; }
    }
}
