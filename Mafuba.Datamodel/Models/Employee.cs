﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mafuba.Datamodel.Models
{
    public class Employee : Basemodel
    {
        public string Cedula { get; set; }
        public string Nombre { get; set; }
        public DateTime EntryDate { get; set; }
        [NotMapped]
        public string Department { get { return Job?.Department; } }
        public int JobId { get; set; }
        public double Salary { get; set; }
        public int CandidateId { get; set; }
        [NotMapped]
        public string JobName { get { return Job?.Name; } }

        public virtual Candidate Candidate { get; set; }
        public virtual Job Job { get; set; }
    }
}
