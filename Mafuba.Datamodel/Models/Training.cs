﻿using Mafuba.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mafuba.Datamodel.Models
{
    public class Training : Basemodel
    {
        public Training()
        {
            Since = DateTime.Today;
            To = DateTime.Today;
        }
        public string Description { get; set; }
        public AcademicLevel Level { get; set; }
        public DateTime Since { get; set; }
        public DateTime To { get; set; }
        public string Institute { get; set; }

        public virtual ICollection<Candidate> Candidates { get; set; }

    }
}
