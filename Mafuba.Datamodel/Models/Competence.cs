﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mafuba.Datamodel.Models
{
    public class Competence : Basemodel
    {
        public string Description { get; set; }
        public virtual ICollection<Candidate> Candidates { get; set; }

    }
}
