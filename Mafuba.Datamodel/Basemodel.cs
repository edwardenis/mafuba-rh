﻿using Mafuba.Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mafuba.Datamodel
{
    public class Basemodel
    {
        [Key]
        public int Id { get; set; }
        public string State { get; set; }
    }
}
