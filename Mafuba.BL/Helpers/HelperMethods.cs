﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Mafuba.BL.Helpers
{
    public class HelperMethods
    {
        public static TProp GetPropertyValue<T, TProp>(T instance, PropertyInfo pi)
        {
            if (instance == null) throw new ArgumentNullException(nameof(instance));

            if (pi == null)
                throw new Exception("No property '{0}' found on the instance of type '{1}'.");

            if (!pi.CanRead)
                throw new Exception("Property '{0}' Cannot be readed.");

            var propertyValue = pi.GetValue(instance);

            return (TProp)Convert.ChangeType(propertyValue, typeof(TProp));
        }
    }
}
