﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mafuba.BL.Dto
{
    public class CompetenceDto : BaseModelDto
    {
        public string Description { get; set; }
    }
}
