﻿using Mafuba.BL.Interfaces;
using Mafuba.Datamodel.Context;
using Mafuba.Datamodel.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Mafuba.BL.Services
{
    public class LoginService : ILoginService
    {
        private readonly MafubaContext _mafubaContext;
        public LoginService(MafubaContext mafubaContext)
        {
            _mafubaContext = mafubaContext;
        }
        public User LogIn(string userName, string password)
        {
            var user = _mafubaContext.Users
                .Where(u => u.Username == userName 
                        && u.Password == password)
                .FirstOrDefault();

            return user;
        }
    }
}
