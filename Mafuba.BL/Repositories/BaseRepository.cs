﻿using Mafuba.BL.Interfaces;
using Mafuba.Datamodel;
using Mafuba.Datamodel.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mafuba.BL.Repositories
{
    public class BaseRepository<T> : IRepository<T> where T : Basemodel
    {
        protected MafubaContext _dbContext;
        protected DbSet<T> _dbSet;
        public BaseRepository(MafubaContext mafubaContext)
        {
            _dbContext = mafubaContext;
            _dbSet = _dbContext.Set<T>();
        }

        public IEnumerable<T> GetAll()
        {
            return _dbSet.ToList();
        }

        public void Add(T entity)
        {
            _dbSet.Add(entity);
        }
        public void Remove(T entity)
        {
            _dbSet.Remove(entity);
        }

        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }

        public BindingList<T> GetBindingList()
        {
            _dbSet.Load();
            var bindingList = _dbSet.Local.ToBindingList();
            return bindingList;
        }

        public IQueryable<T> GetQueryable()
        {
            return _dbSet.AsQueryable();
        }

        public void Update(T entity)
        {
            DbEntityEntry dbEntityEntry = _dbContext.Entry(entity);
            //

            _dbContext.Set<T>().Attach(entity);
            dbEntityEntry.State = EntityState.Modified;
        }
    }
}
