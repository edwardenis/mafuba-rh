﻿using Mafuba.Datamodel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mafuba.BL.Interfaces
{
    public interface ILoginService
    {
        User LogIn(string userName, string password);
    }
}
