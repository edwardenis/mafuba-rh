﻿using Mafuba.Datamodel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mafuba.BL.Interfaces
{
    public interface IRepository<T> where T: Basemodel
    {
        IEnumerable<T> GetAll();
        IQueryable<T> GetQueryable();
        BindingList<T> GetBindingList();
        void Add(T entity);
        void Update(T entity);
        void Remove(T entity);

        void SaveChanges();
    }
}
