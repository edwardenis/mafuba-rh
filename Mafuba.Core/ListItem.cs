﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mafuba.Core
{
    public class ListItem<TValue>
    {
        public string Description { get; set; }
        public TValue Value { get; set; }
    }
}
