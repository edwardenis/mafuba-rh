﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mafuba.Core.Enums
{
    public enum RiskLevel
    {
        LOW,
        MEDIUM,
        HIGH
    }
}
