﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mafuba.Core.Enums
{
    public enum DataState
    {
        ACTIVE = 1,
        INACTIVE = 0
    }
}
