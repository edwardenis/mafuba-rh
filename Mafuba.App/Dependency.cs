﻿using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mafuba.App
{
    public static class Dependency
    {
        private static Container _container;
        
        public static Object GetInstanceOf(Type type)
        {
            return GetContainer().GetInstance(type);
        }
        private static Container GetContainer()
        {
            if (_container == null)
            {
                _container = new Container();
                _container.RegisterMafubaInjection();
            }

            return _container;
                
        }

        public static T GetInstanceOf<T>()
        {
            return (T)GetInstanceOf(typeof(T));
        }

        public static void SetContainer(Container container)
        {
            if (_container == null)
                _container = container;
        }
    }
}
