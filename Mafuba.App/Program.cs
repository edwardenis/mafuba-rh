﻿using Mafuba.App.Forms;
using Mafuba.Datamodel.Models;
using System;
using System.Windows.Forms;

namespace Mafuba.App
{
    static class Program
    {
        public static User LoggedUser;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //
            Application.Run(Dependency.GetInstanceOf<HomeForm>());
        }
    }
}
