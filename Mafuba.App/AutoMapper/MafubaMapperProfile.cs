﻿using AutoMapper;
using Mafuba.App.Forms;
using Mafuba.App.Forms.Candidate;
using Mafuba.App.Forms.Competence;
using Mafuba.App.Forms.Job;
using Mafuba.App.Forms.Lenguage;
using Mafuba.App.Forms.WorkExperience;
using Mafuba.BL.Dto;
using Mafuba.Core.Enums;
using Mafuba.Datamodel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mafuba.App.AutoMapper
{
    public class MafubaMapperProfile : Profile
    {
        public MafubaMapperProfile()
        {
            //CreateMap<CompetenceForm, Competence>()
            //    .ForMember(c => c.Description, cfg => cfg.MapFrom(f => f.Descripcion))
            //    .ForMember(c => c.State, cfg => cfg.MapFrom(f => f.Estado))
            //.ReverseMap();
            CreateMap<CompetenceForm, Competence>()
                .ForMember(e => e.Description, cfg => cfg.MapFrom(f => f.tbxDescripcion.Text))
                .ForMember(e => e.State, cfg => cfg.MapFrom(f => f.cbxEstado.SelectedItem.ToString()))
                .ReverseMap();

            CreateMap<Competence, Competence>().ReverseMap();
            //
          
            CreateMap<LenguageForm, Lenguage>()
                .ForMember(e => e.Name, cfg => cfg.MapFrom(f => f.tbxNombre.Text))
                .ForMember(e => e.State, cfg => cfg.MapFrom(f => f.cbxEstado.SelectedItem.ToString()))
                .ReverseMap();
            CreateMap<Lenguage, Lenguage>().ReverseMap();
            //
            CreateMap<TrainingsForm, Training>()
                .ForMember(t => t.Description, cfg => cfg.MapFrom(f => f.tbxDescripcion.Text))
                .ForMember(t => t.Institute, cfg => cfg.MapFrom(f => f.tbxInstitucion.Text))
                .ForMember(t => t.Since, cfg => cfg.MapFrom(f => f.dtpDesde.Value))
                .ForMember(t => t.To, cfg => cfg.MapFrom(f => f.dtpHasta.Value))
                .ForMember(t => t.State, cfg => cfg.MapFrom(f => f.cbxEstado.SelectedItem.ToString()))
                .ForMember(t => t.Level, 
                    cfg => cfg.MapFrom(f => Enum.Parse(typeof(AcademicLevel), f.cbxNivel.SelectedValue.ToString())))
                .ReverseMap();

            CreateMap<Training, Training>();
            //
            CreateMap<JobForm, Job>()
                .ForMember(e => e.Name, cfg => cfg.MapFrom(f => f.tbxName.Text))
                .ForMember(e => e.Department, cfg => cfg.MapFrom(f => f.tbxDepartamento.Text))
                .ForMember(e => e.RiskLevel, cfg => cfg.MapFrom(f => Enum.Parse(typeof(RiskLevel), f.cbxRiskLevel.SelectedValue.ToString())))
                .ForMember(e => e.LowestSalary, cfg => cfg.MapFrom(f => f.tbxMinSalary.Text))
                .ForMember(e => e.MaxSalary, cfg => cfg.MapFrom(f => f.tbxMaxSalary.Text))
                .ForMember(e => e.State, cfg => cfg.MapFrom(f => f.cbxEstado.SelectedItem.ToString()))
                .ReverseMap();

            CreateMap<Job, Job>().ReverseMap();
            //
            CreateMap<NewWorkExperienceForm, WorkExperience>()
                .ForMember(w => w.Company, cfg => cfg.MapFrom(f => f.tbxEmpresa.Text))
                .ForMember(w => w.PositionOcuppied, cfg => cfg.MapFrom(f => f.tbxPuesto.Text))
                .ForMember(w => w.Salary, cfg => cfg.MapFrom(f => f.tbxSalario.Text))
                .ForMember(w => w.Since, cfg => cfg.MapFrom(f => f.dtpDesde.Value))
                .ForMember(w => w.To, cfg => cfg.MapFrom(f => f.dtpHasta.Value))
                .ReverseMap();
            //
            CreateMap<NewCandidateForm, Candidate>()
                .ForMember(w => w.Cedula, cfg => cfg.MapFrom(f => f.tbxCedula.Text))
                .ForMember(w => w.Name, cfg => cfg.MapFrom(f => f.tbxName.Text))
                .ForMember(w => w.AspirationSalary, cfg => cfg.MapFrom(f => f.tbxAspirationSalary.Text))
                .ForMember(w => w.RecommendedBy, cfg => cfg.MapFrom(f => f.tbxRecomendacion.Text))
                .ForMember(w => w.PrincipalCompetencies, cfg => cfg.MapFrom(f => f.listBoxCompetencias.SelectedItems))
                .ForMember(w => w.PrincpialTrainings, cfg => cfg.MapFrom(f => f.listBoxCapacitaciones.SelectedItems))
                .ReverseMap()
                .ForMember(f => f.listBoxCompetencias, cfg => cfg.Ignore())
                .ForMember(f => f.listBoxCapacitaciones, cfg => cfg.Ignore());

            CreateMap<Candidate, Candidate>();
            CreateMap<Candidate, Employee>()
                .ForMember(e => e.JobId, cfg => cfg.MapFrom(c => c.AspirationJobId))
                .ForMember(e => e.Department, cfg => cfg.MapFrom(c => c.AspirationJob.Department))
                .ForMember(e => e.Nombre, cfg => cfg.MapFrom(c => c.Name))
                .ForMember(e => e.CandidateId, cfg => cfg.MapFrom(c => c.Id))
                .ForMember(e => e.Id, cfg => cfg.Ignore())
                .ReverseMap();
        }
    }
}
