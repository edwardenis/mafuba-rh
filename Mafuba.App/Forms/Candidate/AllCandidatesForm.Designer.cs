﻿namespace Mafuba.App.Forms.Candidate
{
    partial class AllCandidatesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnContratar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tblJobs = new System.Windows.Forms.DataGridView();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.tbxSearch = new System.Windows.Forms.TextBox();
            this.btnIgnorar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.tblJobs)).BeginInit();
            this.SuspendLayout();
            // 
            // btnContratar
            // 
            this.btnContratar.Location = new System.Drawing.Point(449, 262);
            this.btnContratar.Name = "btnContratar";
            this.btnContratar.Size = new System.Drawing.Size(75, 23);
            this.btnContratar.TabIndex = 9;
            this.btnContratar.Text = "Contratar";
            this.btnContratar.UseVisualStyleBackColor = true;
            this.btnContratar.Click += new System.EventHandler(this.btnContratar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(20, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(208, 16);
            this.label1.TabIndex = 8;
            this.label1.Text = "Estos son los nuevos candidatos:";
            // 
            // tblJobs
            // 
            this.tblJobs.AllowUserToAddRows = false;
            this.tblJobs.AllowUserToDeleteRows = false;
            this.tblJobs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tblJobs.Location = new System.Drawing.Point(23, 67);
            this.tblJobs.Name = "tblJobs";
            this.tblJobs.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.tblJobs.Size = new System.Drawing.Size(502, 179);
            this.tblJobs.TabIndex = 7;
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(449, 9);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(75, 23);
            this.btnBuscar.TabIndex = 6;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // tbxSearch
            // 
            this.tbxSearch.Location = new System.Drawing.Point(23, 12);
            this.tbxSearch.Name = "tbxSearch";
            this.tbxSearch.Size = new System.Drawing.Size(402, 20);
            this.tbxSearch.TabIndex = 5;
            this.tbxSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbxSearch_KeyDown);
            // 
            // btnIgnorar
            // 
            this.btnIgnorar.Location = new System.Drawing.Point(359, 262);
            this.btnIgnorar.Name = "btnIgnorar";
            this.btnIgnorar.Size = new System.Drawing.Size(75, 23);
            this.btnIgnorar.TabIndex = 10;
            this.btnIgnorar.Text = "Ignorar";
            this.btnIgnorar.UseVisualStyleBackColor = true;
            // 
            // AllCandidatesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(544, 297);
            this.Controls.Add(this.btnIgnorar);
            this.Controls.Add(this.btnContratar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tblJobs);
            this.Controls.Add(this.btnBuscar);
            this.Controls.Add(this.tbxSearch);
            this.Name = "AllCandidatesForm";
            this.Text = "AllCandidatesForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AllCandidatesForm_FormClosed);
            this.Load += new System.EventHandler(this.AllCandidatesForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tblJobs)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnContratar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView tblJobs;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.TextBox tbxSearch;
        private System.Windows.Forms.Button btnIgnorar;
    }
}