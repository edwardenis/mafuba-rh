﻿using AutoMapper;
using Mafuba.BL.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mafuba.App.Forms.Candidate
{
    public partial class ContractForm : Form
    {
        private Datamodel.Models.Candidate _candidate;
        private IMapper _mapper;
        private IRepository<Datamodel.Models.Employee> _employeeRepository;
        private AllCandidatesForm _allCandidatesForm;
        public ContractForm(Datamodel.Models.Candidate candidate, AllCandidatesForm allCandidatesForm)
        {
            _mapper = Dependency.GetInstanceOf<IMapper>();
            _employeeRepository = Dependency.GetInstanceOf<IRepository<Datamodel.Models.Employee>>();
            _candidate = candidate;
            _allCandidatesForm = allCandidatesForm;
            InitializeComponent();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            var employee = _mapper.Map<Datamodel.Models.Employee>(_candidate);
            employee.EntryDate = DateTime.Now;
            employee.State = "ACTIVO";
            employee.Salary = double.Parse(tbxSalario.Text);

            _candidate.State = "INACTIVO";
            _candidate.AspirationJob.State = "INACTIVO";
            _employeeRepository.Add(employee);
            _employeeRepository.SaveChanges();
            _allCandidatesForm._repository.SaveChanges();

            _allCandidatesForm.LoadTableData();
            this.Close();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tbxSalario_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void tbxSalario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) ||
                char.IsSymbol(e.KeyChar) ||
                char.IsWhiteSpace(e.KeyChar) ||
                (char.IsPunctuation(e.KeyChar) && (tbxSalario.Text.Contains(".") || e.KeyChar != '.')))
                e.Handled = true;
        }
    }
}
