﻿namespace Mafuba.App.Forms.Candidate
{
    partial class NewCandidateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbxRecomendacion = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.listBoxCapacitaciones = new System.Windows.Forms.ListBox();
            this.trainingBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.listBoxCompetencias = new System.Windows.Forms.ListBox();
            this.competenceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tbxAspirationSalary = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbxName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbxCedula = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnRemoveWork = new System.Windows.Forms.Button();
            this.btnNewWork = new System.Windows.Forms.Button();
            this.tblWorkExperience = new System.Windows.Forms.DataGridView();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trainingBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.competenceBindingSource)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblWorkExperience)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbxRecomendacion);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.listBoxCapacitaciones);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.listBoxCompetencias);
            this.groupBox1.Controls.Add(this.tbxAspirationSalary);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.tbxName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tbxCedula);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(32, 21);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(625, 165);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Información del candidato";
            // 
            // tbxRecomendacion
            // 
            this.tbxRecomendacion.Location = new System.Drawing.Point(479, 48);
            this.tbxRecomendacion.Name = "tbxRecomendacion";
            this.tbxRecomendacion.Size = new System.Drawing.Size(130, 20);
            this.tbxRecomendacion.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(476, 31);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Recomendado por:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(314, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Capacitaciones:";
            // 
            // listBoxCapacitaciones
            // 
            this.listBoxCapacitaciones.DataSource = this.trainingBindingSource;
            this.listBoxCapacitaciones.DisplayMember = "Description";
            this.listBoxCapacitaciones.FormattingEnabled = true;
            this.listBoxCapacitaciones.Location = new System.Drawing.Point(317, 92);
            this.listBoxCapacitaciones.Name = "listBoxCapacitaciones";
            this.listBoxCapacitaciones.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.listBoxCapacitaciones.Size = new System.Drawing.Size(292, 56);
            this.listBoxCapacitaciones.TabIndex = 8;
            this.listBoxCapacitaciones.ValueMember = "Id";
            // 
            // trainingBindingSource
            // 
            this.trainingBindingSource.DataSource = typeof(Mafuba.Datamodel.Models.Training);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Competencias:";
            // 
            // listBoxCompetencias
            // 
            this.listBoxCompetencias.DataSource = this.competenceBindingSource;
            this.listBoxCompetencias.DisplayMember = "Description";
            this.listBoxCompetencias.FormattingEnabled = true;
            this.listBoxCompetencias.Location = new System.Drawing.Point(10, 92);
            this.listBoxCompetencias.Name = "listBoxCompetencias";
            this.listBoxCompetencias.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBoxCompetencias.Size = new System.Drawing.Size(283, 56);
            this.listBoxCompetencias.TabIndex = 6;
            this.listBoxCompetencias.ValueMember = "Id";
            // 
            // competenceBindingSource
            // 
            this.competenceBindingSource.DataSource = typeof(Mafuba.Datamodel.Models.Competence);
            // 
            // tbxAspirationSalary
            // 
            this.tbxAspirationSalary.Location = new System.Drawing.Point(317, 48);
            this.tbxAspirationSalary.Name = "tbxAspirationSalary";
            this.tbxAspirationSalary.Size = new System.Drawing.Size(130, 20);
            this.tbxAspirationSalary.TabIndex = 5;
            this.tbxAspirationSalary.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxAspirationSalary_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(314, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Salario al que aspira:";
            // 
            // tbxName
            // 
            this.tbxName.Location = new System.Drawing.Point(163, 48);
            this.tbxName.Name = "tbxName";
            this.tbxName.Size = new System.Drawing.Size(130, 20);
            this.tbxName.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(160, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nombre:";
            // 
            // tbxCedula
            // 
            this.tbxCedula.Location = new System.Drawing.Point(10, 48);
            this.tbxCedula.Name = "tbxCedula";
            this.tbxCedula.Size = new System.Drawing.Size(130, 20);
            this.tbxCedula.TabIndex = 1;
            this.tbxCedula.Leave += new System.EventHandler(this.tbxCedula_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Cédula:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnRemoveWork);
            this.groupBox2.Controls.Add(this.btnNewWork);
            this.groupBox2.Controls.Add(this.tblWorkExperience);
            this.groupBox2.Location = new System.Drawing.Point(32, 192);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(625, 164);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Experiencia laboral";
            // 
            // btnRemoveWork
            // 
            this.btnRemoveWork.Location = new System.Drawing.Point(463, 128);
            this.btnRemoveWork.Name = "btnRemoveWork";
            this.btnRemoveWork.Size = new System.Drawing.Size(75, 23);
            this.btnRemoveWork.TabIndex = 2;
            this.btnRemoveWork.Text = "Remover";
            this.btnRemoveWork.UseVisualStyleBackColor = true;
            this.btnRemoveWork.Click += new System.EventHandler(this.btnRemoveWork_Click);
            // 
            // btnNewWork
            // 
            this.btnNewWork.Location = new System.Drawing.Point(544, 128);
            this.btnNewWork.Name = "btnNewWork";
            this.btnNewWork.Size = new System.Drawing.Size(75, 23);
            this.btnNewWork.TabIndex = 1;
            this.btnNewWork.Text = "Agregar";
            this.btnNewWork.UseVisualStyleBackColor = true;
            this.btnNewWork.Click += new System.EventHandler(this.btnNewWork_Click);
            // 
            // tblWorkExperience
            // 
            this.tblWorkExperience.AllowUserToAddRows = false;
            this.tblWorkExperience.AllowUserToDeleteRows = false;
            this.tblWorkExperience.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tblWorkExperience.Location = new System.Drawing.Point(6, 19);
            this.tblWorkExperience.Name = "tblWorkExperience";
            this.tblWorkExperience.ReadOnly = true;
            this.tblWorkExperience.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullColumnSelect;
            this.tblWorkExperience.Size = new System.Drawing.Size(613, 103);
            this.tblWorkExperience.TabIndex = 0;
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(575, 398);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(75, 23);
            this.btnGuardar.TabIndex = 13;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(495, 398);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 14;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // NewCandidateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(695, 446);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "NewCandidateForm";
            this.Text = "NewCandidateForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.NewCandidateForm_FormClosed);
            this.Load += new System.EventHandler(this.NewCandidateForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trainingBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.competenceBindingSource)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tblWorkExperience)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.TextBox tbxName;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox tbxCedula;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox tbxAspirationSalary;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox tbxRecomendacion;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.ListBox listBoxCapacitaciones;
        public System.Windows.Forms.BindingSource trainingBindingSource;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.ListBox listBoxCompetencias;
        public System.Windows.Forms.BindingSource competenceBindingSource;
        public System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.DataGridView tblWorkExperience;
        public System.Windows.Forms.Button btnRemoveWork;
        public System.Windows.Forms.Button btnNewWork;
        public System.Windows.Forms.Button btnGuardar;
        public System.Windows.Forms.Button btnCancelar;
    }
}