﻿using Mafuba.App.Service;
using Mafuba.BL.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mafuba.App.Forms.Candidate
{
    public partial class AllCandidatesForm : Form
    {
        private TableService _tableService;
        public IRepository<Datamodel.Models.Candidate> _repository;
        private AdministrationHomeForm _parentForm;
        public AllCandidatesForm(AdministrationHomeForm administrationHomeForm)
        {
            _tableService = Dependency.GetInstanceOf<TableService>();
            _repository = Dependency.GetInstanceOf<IRepository<Datamodel.Models.Candidate>>();
            _parentForm = administrationHomeForm;
            InitializeComponent();
        }

        private void AllCandidatesForm_Load(object sender, EventArgs e)
        {
            _parentForm.Hide();
            _tableService.BuildTable(tblJobs, new GridColumn
            {
                ColumnDataName = "Cedula",
                ColumnName = "Cedula"
            }, new GridColumn
            {
                ColumnDataName = "Name",
                ColumnName = "Nombre"
            }, new GridColumn
            {
                ColumnDataName = "AspirationJobName",
                ColumnName = "Puesto"
            }, new GridColumn
            {
                ColumnDataName = "AspirationSalary",
                ColumnName = "Salario aspirado"
            });
            LoadTableData();
        }

        public void LoadTableData()
        {

            var list = _repository.GetQueryable()
                .Where(x => x.State.ToUpper() == "ACTIVO")
                .ToList();
            //
            tblJobs.DataSource = new BindingList<Datamodel.Models.Candidate>(list);

        }

        private void AllCandidatesForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            _parentForm.Show();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            var searchTerm = tbxSearch.Text;

            var result = _repository.GetQueryable()
                .Where(j => j.State.ToUpper() == "ACTIVO" && (j.Cedula.StartsWith(searchTerm)
                        || j.Name.ToString().StartsWith(searchTerm)
                        || j.AspirationJob.Name.StartsWith(searchTerm)
                        || j.RecommendedBy.StartsWith(searchTerm)
                        || j.PrincipalCompetencies.Any(c => c.Description.StartsWith(searchTerm))))
                .ToList();

            tblJobs.DataSource = result;
        }

        private void tbxSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                this.btnBuscar.PerformClick();
            }
        }

        private void btnContratar_Click(object sender, EventArgs e)
        {
            var cndidate = tblJobs.CurrentRow.DataBoundItem as Datamodel.Models.Candidate;
            var contractForm = new ContractForm(cndidate, this);
            contractForm.Show();
        }
    }
}
