﻿using AutoMapper;
using Mafuba.App.Forms.Job;
using Mafuba.App.Service;
using Mafuba.App.Services;
using Mafuba.BL.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mafuba.App.Forms.Candidate
{
    public partial class NewCandidateForm : Form
    {
        private IRepository<Datamodel.Models.Candidate> _candidateRepository;
        private IRepository<Datamodel.Models.Competence> _competenceRepository;
        private IRepository<Datamodel.Models.Training> _trainingRepository;
        private IMapper _mapper;
        public EmailService _emailService;
        private AllJobsForm _parentForm;
        public Datamodel.Models.Candidate _candidate;
        public Datamodel.Models.Job _aspiredJob;
        public NewCandidateForm(AllJobsForm allJobsForm, Datamodel.Models.Job job)
        {
            _parentForm = allJobsForm;
            _candidateRepository = Dependency.GetInstanceOf<IRepository<Datamodel.Models.Candidate>>();
            _competenceRepository = Dependency.GetInstanceOf<IRepository<Datamodel.Models.Competence>>();
            _trainingRepository = Dependency.GetInstanceOf<IRepository<Datamodel.Models.Training>>();
            _mapper = Dependency.GetInstanceOf<IMapper>();
            var appSettings = ConfigurationManager.AppSettings;
            _emailService = new EmailService(appSettings["SmtpServerHost"],
                                            int.Parse(appSettings["SmtpServerPort"]),
                                            appSettings["SmtpServerUsername"],
                                            appSettings["SmtpServerPassword"],
                                            bool.Parse(appSettings["SmtpServerUseSsl"]));
            _candidate = new Datamodel.Models.Candidate();
            _candidate.AspirationJobId = job.Id;
            _aspiredJob = job;
            InitializeComponent();
        }

        private void NewCandidateForm_Load(object sender, EventArgs e)
        {
            InitTable();
            competenceBindingSource.DataSource = _competenceRepository.GetBindingList();
            trainingBindingSource.DataSource = _trainingRepository.GetBindingList();

        }
        private void InitTable()
        {
            var tableService = Dependency.GetInstanceOf<TableService>();
            //
            tableService.BuildTable(tblWorkExperience, new GridColumn
            {
                ColumnDataName = "Company",
                ColumnName = "Compañia"
            }, new GridColumn
            {
                ColumnDataName = "PositionOcuppied",
                ColumnName = "Posición ocupada"
            }, new GridColumn
            {
                ColumnDataName = "Salary",
                ColumnName = "Salario"
            }, new GridColumn
            {
                ColumnDataName = "Since",
                ColumnName = "Desde"
            }, new GridColumn
            {
                ColumnDataName = "To",
                ColumnName = "Hasta"
            });

            LoadTableData();

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            _parentForm.Show();
            this.Dispose();
        }

        public void LoadTableData()
        {
            tblWorkExperience.DataSource = _candidate.WorkExperiences.ToList();
        }

        private void NewCandidateForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            _parentForm.Show();
        }

        private void btnNewWork_Click(object sender, EventArgs e)
        {
            var newWork = new WorkExperience.NewWorkExperienceForm(this);
            newWork.Show();
        }
        

        private void btnRemoveWork_Click(object sender, EventArgs e)
        {
            var work = tblWorkExperience.CurrentRow.DataBoundItem as Datamodel.Models.WorkExperience;
            if (work != null)
                _candidate.WorkExperiences.Remove(work);
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            _mapper.Map(this, _candidate);
            _candidate.State = "ACTIVO";
            _candidateRepository.Add(_candidate);
            _candidateRepository.SaveChanges();
            MessageBox.Show("Su solicitud ha sido enviada.");
            SendEmail();
            this.Close();
        }

        private async Task SendEmail()
        {
            var templateModel = new
            {
                 _candidate.Cedula,
                Nombre = _candidate.Name,
                Puesto = _aspiredJob.Name,
                SueldoDeseado = _candidate.AspirationSalary,
                SalarioMaximo = _aspiredJob.MaxSalary
            };
            var templatePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Templates\\UserSuggestionTemplate.html");
            await _emailService
                    .To(ConfigurationManager.AppSettings["MafubaAdministrator"])
                    .Subject($"Un nuevo candidato para el puesto de {_aspiredJob.Name}")
                    .UseTemplate(templatePath, true, templateModel)

                    //.UseTemplate(HostingEnvironment.MapPath(@"~/Templates/UserSuggestionTemplate.html"), true, emailModel)
                    //.Body($"Hay un nuevo candidato para el puesto de: {_aspiredJob.Name}", false)
                    .SendAsync();
        }

        private void tbxCedula_TextChanged(object sender, EventArgs e)
        {

        }

        private void tbxAspirationSalary_TextChanged(object sender, EventArgs e)
        {

        }

        private void tbxAspirationSalary_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) ||
                char.IsSymbol(e.KeyChar) ||
                char.IsWhiteSpace(e.KeyChar) ||
                (char.IsPunctuation(e.KeyChar) && (tbxAspirationSalary.Text.Contains(".") || e.KeyChar != '.')))
                e.Handled = true;
        }

        private void tbxCedula_Leave(object sender, EventArgs e)
        {
            var cedula = tbxCedula.Text;
            var tieneSymbolos = cedula.Contains("-");
            var isValid = HelperMetthods.VerificarCedula(cedula, tieneSymbolos);
            if (!isValid)
            {
                MessageBox.Show("Esta cedula es invalida!");
                tbxCedula.Focus();
                return;
            }

            var candidate = _candidateRepository
                            .GetQueryable()
                            .Where(c => string.Equals(c.Cedula, cedula)).FirstOrDefault();
            if (candidate != null)
            {

                _mapper.Map(candidate, _candidate);
                _mapper.Map(_candidate, this);

                if (candidate.Employees != null && candidate.Employees.Any())
                {
                    var employee = candidate.Employees.FirstOrDefault();
                    if (employee.State.Equals("ACTIVO", StringComparison.InvariantCulture))
                        MessageBox.Show($"Esta persona ya tiene un puesto como {employee.Job.Name}");
                }
                LoadTableData();


            }



        }

        
    }
}
