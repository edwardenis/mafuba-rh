﻿using AutoMapper;
using Mafuba.App.Service;
using Mafuba.BL;
using Mafuba.BL.Interfaces;
using Mafuba.Datamodel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mafuba.App.Forms
{
    public partial class BaseMaintenanceForm<T>: Form where T:Basemodel, new()
    {
        private T _currentEntity { get; set; }
        private IRepository<T> _repository;
        private IMapper _mapper;
        private GridColumn[] _columnsInformation;
        private TableService _tableService;
        public BaseMaintenanceForm()
        {
            _repository = Dependency.GetInstanceOf<IRepository<T>>();
            _mapper = Dependency.GetInstanceOf<IMapper>();
            _tableService = Dependency.GetInstanceOf<TableService>();
            _currentEntity = new T();
            InitializeComponent();
        }
        public void RegisterTableColumns(params GridColumn[] columnsInformation)
        {
            _columnsInformation = columnsInformation;
        }

        protected void BuildTable()
        {
             _tableService.BuildTable(tableGrid,_columnsInformation);
            //
            tableGrid.DataSource = _repository.GetBindingList();
            
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            MapFormValueToCurrentEntity();

            if (_currentEntity.Id == 0)
            {
                _repository.Add(_currentEntity);
            }

            _repository.SaveChanges();
            CleanCurrentEntity();
            MessageBox.Show(this, "Guardado Exitoso", "Info");
            
        }
        

        private void CleanCurrentEntity()
        {
            _currentEntity = new T();
            MapCurrentEntityValuesToForm();
        }

        private void MapFormValueToCurrentEntity() {
            _mapper.Map(this, _currentEntity);
        }

        private void MapCurrentEntityValuesToForm()
        {
            _mapper.Map(_currentEntity, this);
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            CleanCurrentEntity();
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox
                    .Show("¿Estás seguro que deseas eliminar este registro?", "Confirmación", MessageBoxButtons.YesNo);

            if (confirmResult == DialogResult.No)
                return;

            var selectedEntity = (T)tableGrid.CurrentRow.DataBoundItem;
            _repository.Remove(selectedEntity);
            _repository.SaveChanges();

            MessageBox.Show(this, "Borrado Exitoso", "Info");


        }
    }
}
