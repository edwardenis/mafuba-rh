﻿using AutoMapper;
using Mafuba.App.Forms.Candidate;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mafuba.App.Forms.WorkExperience
{
    public partial class NewWorkExperienceForm : Form
    {
        private Datamodel.Models.Candidate _candidate;
        private Datamodel.Models.WorkExperience _workExperience;
        private NewCandidateForm _newCandidateForm;
        private IMapper _mapper;
        public NewWorkExperienceForm(NewCandidateForm newCandidateForm)
        {
            _mapper = Dependency.GetInstanceOf<IMapper>();
            _newCandidateForm = newCandidateForm;
            _candidate = newCandidateForm._candidate;
            _workExperience = new Datamodel.Models.WorkExperience();
            InitializeComponent();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            var fechaDesde = dtpDesde.Value;
            var fechaHasta = dtpHasta.Value;
            if (fechaDesde > fechaHasta)
            {
                MessageBox.Show("La fecha desde no puede ser mayor que la fecha hasta.");
                return;
            }
            _mapper.Map(this, _workExperience);
            _candidate.WorkExperiences.Add(_workExperience);
            _newCandidateForm.LoadTableData();
            this.Close();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void NewWorkExperienceForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            _newCandidateForm.Focus();
        }

        private void tbxSalario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) ||
                char.IsSymbol(e.KeyChar) ||
                char.IsWhiteSpace(e.KeyChar) ||
                (char.IsPunctuation(e.KeyChar) && (tbxSalario.Text.Contains(".") || e.KeyChar != '.')))
                        e.Handled = true;
        }
    }
}
