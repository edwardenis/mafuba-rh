﻿using Mafuba.BL.Interfaces;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mafuba.App.Forms
{
    public partial class ReportForm : Form
    {
        private IRepository<Datamodel.Models.Employee> _repository;
        public ReportForm()
        {
            _repository = Dependency.GetInstanceOf<IRepository<Datamodel.Models.Employee>>();
            InitializeComponent();
        }

        private void ReportForm_Load(object sender, EventArgs e)
        {

            this.btnLoad.PerformClick();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            var desde = dtpDesde.Value;
            var hasta = dtpHasta.Value;

            var empleyees = _repository.GetQueryable()
                        .Where(em => em.EntryDate > desde && em.EntryDate <= hasta)
                        .ToList();

            ReportParameter[] parameters = new ReportParameter[]
            {
                new ReportParameter("FechaDesde", desde.Date.ToShortDateString()),
                new ReportParameter("FechaHasta", hasta.Date.ToShortDateString())
            };

            EmployeeBindingSource.DataSource = empleyees;
            reportViewer.LocalReport.SetParameters(parameters);
            reportViewer.RefreshReport();
        }

        private void EmployeeBindingSource_CurrentChanged(object sender, EventArgs e)
        {

        }
    }
}
