﻿using Mafuba.BL.Interfaces;
using Mafuba.Core.Enums;
using Mafuba.Datamodel.Models;
using System;
using System.Windows.Forms;

namespace Mafuba.App.Forms
{
    public partial class TrainingsForm : TrainingFormBase
    {
        public TrainingsForm() : base()
        {
            InitializeComponent();
            cbxNivel.DataSource = Enum.GetValues(typeof(AcademicLevel));
            RegisterTableColumns(new GridColumn
            {
                ColumnDataName = "Description",
                ColumnName = "Descipción"
            }, new GridColumn
            {
                ColumnDataName = "Level",
                ColumnName = "Nivel"
            }, new GridColumn
            {
                ColumnDataName = "Since",
                ColumnName = "Fecha Desde"
            }, new GridColumn
            {
                ColumnDataName = "to",
                ColumnName = "Hasta"
            }, new GridColumn
            {
                ColumnDataName = "Institute",
                ColumnName = "Instituto"
            }, new GridColumn
            {
                ColumnDataName = "State",
                ColumnName = "Estado"
            });
        }

        private void TrainingsForm_Load(object sender, EventArgs e)
        {
            BuildTable();
        }

    

        private void dtpDesde_ValueChanged(object sender, EventArgs e)
        {
            var value = dtpDesde.Value;
            if (value >= dtpHasta.Value)
            {
                MessageBox.Show("La fecha desde debe ser menor que la fecha hasta.");
                dtpDesde.Focus();
            }
        }

        private void dtpHasta_ValueChanged(object sender, EventArgs e)
        {
            var value = dtpHasta.Value;
            if (value <= dtpDesde.Value)
            {
                MessageBox.Show("La fecha hasta debe ser mayor que la fecha desde.");
                dtpHasta.Focus();
            }
        }
    }
}
