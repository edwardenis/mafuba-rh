﻿using Mafuba.Datamodel.Models;

namespace Mafuba.App.Forms
{
    partial class TrainingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDescripcion = new System.Windows.Forms.Label();
            this.tbxDescripcion = new System.Windows.Forms.TextBox();
            this.lblNivel = new System.Windows.Forms.Label();
            this.cbxNivel = new System.Windows.Forms.ComboBox();
            this.dtpDesde = new System.Windows.Forms.DateTimePicker();
            this.lblFechaDesde = new System.Windows.Forms.Label();
            this.lblHasta = new System.Windows.Forms.Label();
            this.dtpHasta = new System.Windows.Forms.DateTimePicker();
            this.lblInstitucion = new System.Windows.Forms.Label();
            this.tbxInstitucion = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbxEstado
            // 
            this.cbxEstado.Location = new System.Drawing.Point(540, 36);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbxInstitucion);
            this.groupBox1.Controls.Add(this.lblInstitucion);
            this.groupBox1.Controls.Add(this.lblHasta);
            this.groupBox1.Controls.Add(this.dtpHasta);
            this.groupBox1.Controls.Add(this.lblFechaDesde);
            this.groupBox1.Controls.Add(this.dtpDesde);
            this.groupBox1.Controls.Add(this.cbxNivel);
            this.groupBox1.Controls.Add(this.lblNivel);
            this.groupBox1.Controls.Add(this.tbxDescripcion);
            this.groupBox1.Controls.Add(this.lblDescripcion);
            this.groupBox1.Size = new System.Drawing.Size(764, 128);
            this.groupBox1.Controls.SetChildIndex(this.label2, 0);
            this.groupBox1.Controls.SetChildIndex(this.cbxEstado, 0);
            this.groupBox1.Controls.SetChildIndex(this.btnGuardar, 0);
            this.groupBox1.Controls.SetChildIndex(this.btnCancelar, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblDescripcion, 0);
            this.groupBox1.Controls.SetChildIndex(this.tbxDescripcion, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblNivel, 0);
            this.groupBox1.Controls.SetChildIndex(this.cbxNivel, 0);
            this.groupBox1.Controls.SetChildIndex(this.dtpDesde, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblFechaDesde, 0);
            this.groupBox1.Controls.SetChildIndex(this.dtpHasta, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblHasta, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblInstitucion, 0);
            this.groupBox1.Controls.SetChildIndex(this.tbxInstitucion, 0);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(537, 21);
            // 
            // lblDescripcion
            // 
            this.lblDescripcion.AutoSize = true;
            this.lblDescripcion.Location = new System.Drawing.Point(17, 20);
            this.lblDescripcion.Name = "lblDescripcion";
            this.lblDescripcion.Size = new System.Drawing.Size(66, 13);
            this.lblDescripcion.TabIndex = 6;
            this.lblDescripcion.Text = "Descripcion:";
            // 
            // tbxDescripcion
            // 
            this.tbxDescripcion.Location = new System.Drawing.Point(20, 37);
            this.tbxDescripcion.Name = "tbxDescripcion";
            this.tbxDescripcion.Size = new System.Drawing.Size(109, 20);
            this.tbxDescripcion.TabIndex = 7;
            // 
            // lblNivel
            // 
            this.lblNivel.AutoSize = true;
            this.lblNivel.Location = new System.Drawing.Point(148, 20);
            this.lblNivel.Name = "lblNivel";
            this.lblNivel.Size = new System.Drawing.Size(34, 13);
            this.lblNivel.TabIndex = 8;
            this.lblNivel.Text = "Nivel:";
            // 
            // cbxNivel
            // 
            this.cbxNivel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxNivel.FormattingEnabled = true;
            this.cbxNivel.Location = new System.Drawing.Point(151, 37);
            this.cbxNivel.Name = "cbxNivel";
            this.cbxNivel.Size = new System.Drawing.Size(109, 21);
            this.cbxNivel.TabIndex = 9;
            // 
            // dtpDesde
            // 
            this.dtpDesde.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDesde.Location = new System.Drawing.Point(286, 38);
            this.dtpDesde.Name = "dtpDesde";
            this.dtpDesde.Size = new System.Drawing.Size(109, 20);
            this.dtpDesde.TabIndex = 10;
            this.dtpDesde.ValueChanged += new System.EventHandler(this.dtpDesde_ValueChanged);
            // 
            // lblFechaDesde
            // 
            this.lblFechaDesde.AutoSize = true;
            this.lblFechaDesde.Location = new System.Drawing.Point(286, 22);
            this.lblFechaDesde.Name = "lblFechaDesde";
            this.lblFechaDesde.Size = new System.Drawing.Size(41, 13);
            this.lblFechaDesde.TabIndex = 11;
            this.lblFechaDesde.Text = "Desde:";
            // 
            // lblHasta
            // 
            this.lblHasta.AutoSize = true;
            this.lblHasta.Location = new System.Drawing.Point(410, 21);
            this.lblHasta.Name = "lblHasta";
            this.lblHasta.Size = new System.Drawing.Size(38, 13);
            this.lblHasta.TabIndex = 13;
            this.lblHasta.Text = "Hasta:";
            // 
            // dtpHasta
            // 
            this.dtpHasta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpHasta.Location = new System.Drawing.Point(410, 37);
            this.dtpHasta.Name = "dtpHasta";
            this.dtpHasta.Size = new System.Drawing.Size(109, 20);
            this.dtpHasta.TabIndex = 12;
            this.dtpHasta.ValueChanged += new System.EventHandler(this.dtpHasta_ValueChanged);
            // 
            // lblInstitucion
            // 
            this.lblInstitucion.AutoSize = true;
            this.lblInstitucion.Location = new System.Drawing.Point(17, 69);
            this.lblInstitucion.Name = "lblInstitucion";
            this.lblInstitucion.Size = new System.Drawing.Size(58, 13);
            this.lblInstitucion.TabIndex = 14;
            this.lblInstitucion.Text = "Institucion:";
            // 
            // tbxInstitucion
            // 
            this.tbxInstitucion.Location = new System.Drawing.Point(20, 86);
            this.tbxInstitucion.Name = "tbxInstitucion";
            this.tbxInstitucion.Size = new System.Drawing.Size(109, 20);
            this.tbxInstitucion.TabIndex = 15;
            // 
            // TrainingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(824, 445);
            this.Name = "TrainingsForm";
            this.Text = "TrainingsForm";
            this.Load += new System.EventHandler(this.TrainingsForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }



        #endregion

        public System.Windows.Forms.ComboBox cbxNivel;
        public System.Windows.Forms.Label lblNivel;
        public System.Windows.Forms.TextBox tbxDescripcion;
        public System.Windows.Forms.Label lblDescripcion;
        public System.Windows.Forms.Label lblInstitucion;
        public System.Windows.Forms.Label lblHasta;
        public System.Windows.Forms.DateTimePicker dtpHasta;
        public System.Windows.Forms.Label lblFechaDesde;
        public System.Windows.Forms.DateTimePicker dtpDesde;
        public System.Windows.Forms.TextBox tbxInstitucion;
    }
}