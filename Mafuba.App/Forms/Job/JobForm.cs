﻿using Mafuba.Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mafuba.App.Forms.Job
{
    public partial class JobForm : JobFormBase
    {
        public JobForm()
        {
            InitializeComponent();
            cbxRiskLevel.DataSource = Enum.GetValues(typeof(RiskLevel));
            RegisterTableColumns(new GridColumn
            {
                ColumnDataName = "Name",
                ColumnName = "Nombre"
            }, new GridColumn
            {
                ColumnDataName = "RiskLevel",
                ColumnName = "Nivel de riego"
            }, new GridColumn
            {
                ColumnDataName = "LowestSalary",
                ColumnName = "Salario minimo"
            }, new GridColumn
            {
                ColumnDataName = "MaxSalary",
                ColumnName = "Salario maximo"
            });
        }

        private void JobForm_Load(object sender, EventArgs e)
        {
            BuildTable();
        }

        private void tbxMinSalary_TextChanged(object sender, EventArgs e)
        {

        }

        private void tbxMaxSalary_TextChanged(object sender, EventArgs e)
        {

        }

        private void tbxMinSalary_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) ||
                char.IsSymbol(e.KeyChar) ||
                char.IsWhiteSpace(e.KeyChar) ||
                (char.IsPunctuation(e.KeyChar) && (tbxMinSalary.Text.Contains(".") || e.KeyChar != '.')))
                e.Handled = true;
        }

        private void tbxMaxSalary_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) ||
                char.IsSymbol(e.KeyChar) ||
                char.IsWhiteSpace(e.KeyChar) ||
                (char.IsPunctuation(e.KeyChar) && (tbxMaxSalary.Text.Contains(".") || e.KeyChar != '.')))
                e.Handled = true;
        }

        private void tbxMinSalary_Leave(object sender, EventArgs e)
        {
            double minSalary = 0;
            double.TryParse(tbxMinSalary.Text, out minSalary);
            double maxSalary = 0;
            double.TryParse(tbxMaxSalary.Text, out maxSalary);

            if (!string.IsNullOrEmpty(tbxMaxSalary.Text) && minSalary >= maxSalary)
            {
                MessageBox.Show("El salario minimo debe ser menor que el salario maximo.");
                tbxMinSalary.Focus();
            }
                
            
        }

        private void tbxMaxSalary_Leave(object sender, EventArgs e)
        {
            double minSalary = 0;
            double.TryParse(tbxMinSalary.Text, out minSalary);
            double maxSalary = 0;
            double.TryParse(tbxMaxSalary.Text, out maxSalary);

            if (maxSalary <= minSalary)
            {
                MessageBox.Show("El salario maximo debe ser mayor que el salario minimo.");
                tbxMaxSalary.Focus();
            }
        }
    }
}
