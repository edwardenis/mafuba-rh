﻿using Mafuba.App.Forms.Candidate;
using Mafuba.App.Service;
using Mafuba.BL.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mafuba.App.Forms.Job
{
    public partial class AllJobsForm : Form
    {
        private TableService _tableService;
        private IRepository<Datamodel.Models.Job> _repository;
        public AllJobsForm()
        {
            _tableService = Dependency.GetInstanceOf<TableService>();
            _repository = Dependency.GetInstanceOf<IRepository<Datamodel.Models.Job>>();
            InitializeComponent();
        }

        private void AllJobsForm_Load(object sender, EventArgs e)
        {
            _tableService.BuildTable(tblJobs, new GridColumn
            {
                ColumnDataName = "Name",
                ColumnName = "Nombre"
            }, new GridColumn
            {
                ColumnDataName = "RiskLevel",
                ColumnName = "Nivel de riego"
            }, new GridColumn
            {
                ColumnDataName = "LowestSalary",
                ColumnName = "Salario minimo"
            }, new GridColumn
            {
                ColumnDataName = "MaxSalary",
                ColumnName = "Salario maximo"
            });

            var list = _repository.GetQueryable()
                            .Where(x => x.State.ToUpper() == "ACTIVO")
                            .ToList();
            //.Where(x => x.State.Equals("ACTIVO", StringComparison.OrdinalIgnoreCase)); ; 

            tblJobs.DataSource = list;
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            var searchTerm = tbxSearch.Text;

            var result = _repository.GetQueryable()
                .Where(j => j.State.ToUpper() == "ACTIVO" && (j.Name.StartsWith(searchTerm)
                        || j.RiskLevel.ToString().StartsWith(searchTerm)
                        || j.LowestSalary.ToString().StartsWith(searchTerm)
                        || j.MaxSalary.ToString().StartsWith(searchTerm)))
                .ToList();

            tblJobs.DataSource = result;

        }

        private void tbxSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                this.btnBuscar.PerformClick();
            }
        }

        private void btnAplicar_Click(object sender, EventArgs e)
        {
            var job = tblJobs.CurrentRow.DataBoundItem as Datamodel.Models.Job;
            var newCandidateForm = new NewCandidateForm(this, job);
            newCandidateForm.Show();
            this.Hide();
        }
    }
}
