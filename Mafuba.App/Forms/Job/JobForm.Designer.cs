﻿namespace Mafuba.App.Forms.Job
{
    partial class JobForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNombre = new System.Windows.Forms.Label();
            this.tbxName = new System.Windows.Forms.TextBox();
            this.cbxRiskLevel = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbxMaxSalary = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbxMinSalary = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tbxDepartamento = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbxEstado
            // 
            this.cbxEstado.Location = new System.Drawing.Point(344, 40);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbxDepartamento);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.tbxMinSalary);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.tbxMaxSalary);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cbxRiskLevel);
            this.groupBox1.Controls.Add(this.tbxName);
            this.groupBox1.Controls.Add(this.lblNombre);
            this.groupBox1.Controls.SetChildIndex(this.btnGuardar, 0);
            this.groupBox1.Controls.SetChildIndex(this.btnCancelar, 0);
            this.groupBox1.Controls.SetChildIndex(this.label2, 0);
            this.groupBox1.Controls.SetChildIndex(this.cbxEstado, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblNombre, 0);
            this.groupBox1.Controls.SetChildIndex(this.tbxName, 0);
            this.groupBox1.Controls.SetChildIndex(this.cbxRiskLevel, 0);
            this.groupBox1.Controls.SetChildIndex(this.label1, 0);
            this.groupBox1.Controls.SetChildIndex(this.label3, 0);
            this.groupBox1.Controls.SetChildIndex(this.tbxMaxSalary, 0);
            this.groupBox1.Controls.SetChildIndex(this.label4, 0);
            this.groupBox1.Controls.SetChildIndex(this.tbxMinSalary, 0);
            this.groupBox1.Controls.SetChildIndex(this.label5, 0);
            this.groupBox1.Controls.SetChildIndex(this.tbxDepartamento, 0);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(341, 24);
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Location = new System.Drawing.Point(21, 24);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(47, 13);
            this.lblNombre.TabIndex = 9;
            this.lblNombre.Text = "Nombre:";
            // 
            // tbxName
            // 
            this.tbxName.Location = new System.Drawing.Point(24, 41);
            this.tbxName.Name = "tbxName";
            this.tbxName.Size = new System.Drawing.Size(116, 20);
            this.tbxName.TabIndex = 10;
            // 
            // cbxRiskLevel
            // 
            this.cbxRiskLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxRiskLevel.FormattingEnabled = true;
            this.cbxRiskLevel.Location = new System.Drawing.Point(178, 40);
            this.cbxRiskLevel.Name = "cbxRiskLevel";
            this.cbxRiskLevel.Size = new System.Drawing.Size(121, 21);
            this.cbxRiskLevel.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(175, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Nivel de riesgo:";
            // 
            // tbxMaxSalary
            // 
            this.tbxMaxSalary.Location = new System.Drawing.Point(178, 84);
            this.tbxMaxSalary.Name = "tbxMaxSalary";
            this.tbxMaxSalary.Size = new System.Drawing.Size(121, 20);
            this.tbxMaxSalary.TabIndex = 14;
            this.tbxMaxSalary.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxMaxSalary_KeyPress);
            this.tbxMaxSalary.Leave += new System.EventHandler(this.tbxMaxSalary_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(175, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Salario Máximo:";
            // 
            // tbxMinSalary
            // 
            this.tbxMinSalary.Location = new System.Drawing.Point(24, 84);
            this.tbxMinSalary.Name = "tbxMinSalary";
            this.tbxMinSalary.Size = new System.Drawing.Size(116, 20);
            this.tbxMinSalary.TabIndex = 16;
            this.tbxMinSalary.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxMinSalary_KeyPress);
            this.tbxMinSalary.Leave += new System.EventHandler(this.tbxMinSalary_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 67);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Salario Mínimo:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(341, 68);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Departamento:";
            // 
            // tbxDepartamento
            // 
            this.tbxDepartamento.Location = new System.Drawing.Point(344, 84);
            this.tbxDepartamento.Name = "tbxDepartamento";
            this.tbxDepartamento.Size = new System.Drawing.Size(121, 20);
            this.tbxDepartamento.TabIndex = 18;
            // 
            // JobForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(824, 445);
            this.Name = "JobForm";
            this.Text = "JobForm";
            this.Load += new System.EventHandler(this.JobForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Label lblNombre;
        public System.Windows.Forms.TextBox tbxName;
        public System.Windows.Forms.TextBox tbxMinSalary;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox tbxMaxSalary;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.ComboBox cbxRiskLevel;
        public System.Windows.Forms.TextBox tbxDepartamento;
        private System.Windows.Forms.Label label5;
    }
}