﻿namespace Mafuba.App.Forms
{
    partial class HomeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnOpenLogin = new System.Windows.Forms.Button();
            this.btnOpenCandidate = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(39, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(292, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "Bienvenido a Mafuba RH";
            // 
            // btnOpenLogin
            // 
            this.btnOpenLogin.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnOpenLogin.Location = new System.Drawing.Point(94, 109);
            this.btnOpenLogin.Name = "btnOpenLogin";
            this.btnOpenLogin.Size = new System.Drawing.Size(175, 42);
            this.btnOpenLogin.TabIndex = 1;
            this.btnOpenLogin.Text = "Acceder como Administrador";
            this.btnOpenLogin.UseVisualStyleBackColor = false;
            this.btnOpenLogin.Click += new System.EventHandler(this.btnOpenLogin_Click);
            // 
            // btnOpenCandidate
            // 
            this.btnOpenCandidate.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnOpenCandidate.Location = new System.Drawing.Point(94, 171);
            this.btnOpenCandidate.Name = "btnOpenCandidate";
            this.btnOpenCandidate.Size = new System.Drawing.Size(175, 42);
            this.btnOpenCandidate.TabIndex = 2;
            this.btnOpenCandidate.Text = "Acceder como invitado";
            this.btnOpenCandidate.UseVisualStyleBackColor = false;
            this.btnOpenCandidate.Click += new System.EventHandler(this.btnOpenCandidate_Click);
            // 
            // HomeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(371, 388);
            this.Controls.Add(this.btnOpenCandidate);
            this.Controls.Add(this.btnOpenLogin);
            this.Controls.Add(this.label1);
            this.Name = "HomeForm";
            this.Text = "HomeForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnOpenLogin;
        private System.Windows.Forms.Button btnOpenCandidate;
    }
}