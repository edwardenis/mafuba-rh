﻿namespace Mafuba.App.Forms.Competence
{
    partial class CompetenceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDescipcion = new System.Windows.Forms.Label();
            this.tbxDescripcion = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbxDescripcion);
            this.groupBox1.Controls.Add(this.lblDescipcion);
            this.groupBox1.Controls.SetChildIndex(this.btnGuardar, 0);
            this.groupBox1.Controls.SetChildIndex(this.btnCancelar, 0);
            this.groupBox1.Controls.SetChildIndex(this.label2, 0);
            this.groupBox1.Controls.SetChildIndex(this.cbxEstado, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblDescipcion, 0);
            this.groupBox1.Controls.SetChildIndex(this.tbxDescripcion, 0);
            // 
            // cbxEstado
            // 
            this.cbxEstado.Location = new System.Drawing.Point(205, 39);
            this.cbxEstado.Size = new System.Drawing.Size(157, 21);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(202, 23);
            // 
            // lblDescipcion
            // 
            this.lblDescipcion.AutoSize = true;
            this.lblDescipcion.Location = new System.Drawing.Point(16, 24);
            this.lblDescipcion.Name = "lblDescipcion";
            this.lblDescipcion.Size = new System.Drawing.Size(66, 13);
            this.lblDescipcion.TabIndex = 9;
            this.lblDescipcion.Text = "Descripción:";
            // 
            // tbxDescripcion
            // 
            this.tbxDescripcion.Location = new System.Drawing.Point(19, 40);
            this.tbxDescripcion.Name = "tbxDescripcion";
            this.tbxDescripcion.Size = new System.Drawing.Size(157, 20);
            this.tbxDescripcion.TabIndex = 10;
            // 
            // CompetenceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(824, 445);
            this.Name = "CompetenceForm";
            this.Text = "CompetenceForm";
            this.Load += new System.EventHandler(this.CompetenceForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Label lblDescipcion;
        public System.Windows.Forms.TextBox tbxDescripcion;
    }
}