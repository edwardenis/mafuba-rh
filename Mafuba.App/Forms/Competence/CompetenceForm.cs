﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mafuba.App.Forms.Competence
{
    public partial class CompetenceForm : CompetenceFormBase
    {
        public CompetenceForm() : base()
        {
            InitializeComponent();
            RegisterTableColumns(new GridColumn
            {
                ColumnDataName = "Description",
                ColumnName = "Descipción"
            },
            new GridColumn
            {
                ColumnDataName = "State",
                ColumnName = "Estado"
            });
        }

        private void CompetenceForm_Load(object sender, EventArgs e)
        {
            BuildTable();
        }
    }
}
