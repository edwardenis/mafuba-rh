﻿using Mafuba.App.Forms;
using Mafuba.App.Forms.Candidate;
using Mafuba.App.Forms.Competence;
using Mafuba.App.Forms.Employee;
using Mafuba.App.Forms.Job;
using Mafuba.App.Forms.Lenguage;
using Mafuba.BL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mafuba.App
{
    public partial class AdministrationHomeForm : Form
    {
        private CompetenceForm _competenciasForm;
        private LenguageForm _lenguageForm;
        private TrainingsForm _trainingsForm;
        private JobForm _jobForm;
        public AdministrationHomeForm()
        {
            InitializeComponent();
        }

        private void HomeForm_Load(object sender, EventArgs e)
        {
            
        }

        private void btnOpenCompetencias_Click(object sender, EventArgs e)
        {
            _competenciasForm = Dependency.GetInstanceOf<CompetenceForm>();
            _competenciasForm.Show();
        }

        private void btnOpenLenguages_Click(object sender, EventArgs e)
        {
            _lenguageForm = Dependency.GetInstanceOf<LenguageForm>();
            _lenguageForm.Show();
        }

        private void btnOpenTrainings_Click(object sender, EventArgs e)
        {
            _trainingsForm = Dependency.GetInstanceOf<TrainingsForm>();
            _trainingsForm.Show();
        }

        private void btnOpenJobs_Click(object sender, EventArgs e)
        {
            _jobForm = Dependency.GetInstanceOf<JobForm>();
            _jobForm.Show();
        }

        private void btnCandidates_Click(object sender, EventArgs e)
        {
            var allCandidates = new AllCandidatesForm(this);
            allCandidates.Show();
        }

        private void btnEmployee_Click(object sender, EventArgs e)
        {
            var allEmployeeForm = Dependency.GetInstanceOf<AllEmployeeForm>();
            allEmployeeForm.Show();
        }

        private void reportsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var reportForm = new ReportForm();
            reportForm.Show();
        }
    }
}
