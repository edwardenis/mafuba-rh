﻿namespace Mafuba.App.Forms.Employee
{
    partial class AllEmployeeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDespedir = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tblEmployees = new System.Windows.Forms.DataGridView();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.tbxSearch = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.tblEmployees)).BeginInit();
            this.SuspendLayout();
            // 
            // btnDespedir
            // 
            this.btnDespedir.Location = new System.Drawing.Point(635, 262);
            this.btnDespedir.Name = "btnDespedir";
            this.btnDespedir.Size = new System.Drawing.Size(75, 23);
            this.btnDespedir.TabIndex = 15;
            this.btnDespedir.Text = "Despedir";
            this.btnDespedir.UseVisualStyleBackColor = true;
            this.btnDespedir.Click += new System.EventHandler(this.btnDespedir_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 16);
            this.label1.TabIndex = 14;
            this.label1.Text = "Empleados registrados:";
            // 
            // tblEmployees
            // 
            this.tblEmployees.AllowUserToAddRows = false;
            this.tblEmployees.AllowUserToDeleteRows = false;
            this.tblEmployees.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tblEmployees.Location = new System.Drawing.Point(12, 67);
            this.tblEmployees.Name = "tblEmployees";
            this.tblEmployees.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.tblEmployees.Size = new System.Drawing.Size(698, 179);
            this.tblEmployees.TabIndex = 13;
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(635, 12);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(75, 23);
            this.btnBuscar.TabIndex = 12;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // tbxSearch
            // 
            this.tbxSearch.Location = new System.Drawing.Point(12, 12);
            this.tbxSearch.Name = "tbxSearch";
            this.tbxSearch.Size = new System.Drawing.Size(599, 20);
            this.tbxSearch.TabIndex = 11;
            // 
            // AllEmployeeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(734, 308);
            this.Controls.Add(this.btnDespedir);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tblEmployees);
            this.Controls.Add(this.btnBuscar);
            this.Controls.Add(this.tbxSearch);
            this.Name = "AllEmployeeForm";
            this.Text = "AllEmployeeForm";
            this.Load += new System.EventHandler(this.AllEmployeeForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tblEmployees)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnDespedir;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView tblEmployees;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.TextBox tbxSearch;
    }
}