﻿using Mafuba.App.Service;
using Mafuba.BL.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mafuba.App.Forms.Employee
{
    public partial class AllEmployeeForm : Form
    {
        private IRepository<Datamodel.Models.Employee> _repository;
        private TableService _tableService;

        public AllEmployeeForm(IRepository<Datamodel.Models.Employee> repository)
        {
            _repository = repository;
            _tableService = Dependency.GetInstanceOf<TableService>();
            InitializeComponent();
        }

        private void AllEmployeeForm_Load(object sender, EventArgs e)
        {
            _tableService.BuildTable(tblEmployees, new GridColumn
            {
                ColumnDataName = "Cedula",
                ColumnName = "Cedula"
            }, new GridColumn
            {
                ColumnDataName = "Nombre",
                ColumnName = "Nombre"
            }, new GridColumn
            {
                ColumnDataName = "EntryDate",
                ColumnName = "Fecha de Entrada"
            },new GridColumn
            {
                ColumnDataName = "Department",
                ColumnName = "Departamento"
            }, new GridColumn
            {
                ColumnDataName = "JobName",
                ColumnName = "Puesto"
            }, new GridColumn
            {
                ColumnDataName = "Salary",
                ColumnName = "Salario"
            });
            tblEmployees.DataSource = _repository.GetBindingList();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            var searchTerm = tbxSearch.Text;

            var result = _repository.GetQueryable()
                .Where(j => j.State.ToUpper() == "ACTIVO" && (j.Cedula.StartsWith(searchTerm)
                        || j.EntryDate.ToString().StartsWith(searchTerm)
                        || j.Job.Name.ToString().StartsWith(searchTerm)
                        || j.Nombre.ToString().StartsWith(searchTerm)
                        || j.Salary.ToString().StartsWith(searchTerm)
                        || j.State.ToString().StartsWith(searchTerm)))
                .ToList();

            tblEmployees.DataSource = result;
        }

        private void btnDespedir_Click(object sender, EventArgs e)
        {
            var confirmacion = MessageBox
                            .Show("Estas seguro que deseas despedir a este Empleado?", "Aviso", MessageBoxButtons.YesNo);

            if (confirmacion == DialogResult.Yes)
            {
                var employee = tblEmployees.CurrentRow.DataBoundItem as Datamodel.Models.Employee;
                employee.Job.State = "ACTIVO";
                employee.State = "INACTIVO";
                _repository.SaveChanges();

            }
        }
    }
}
