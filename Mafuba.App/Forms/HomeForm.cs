﻿using Mafuba.App.Forms.Job;
using Mafuba.Datamodel.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mafuba.App.Forms
{
    public partial class HomeForm : Form
    {
        public HomeForm()
        {
            InitializeComponent();
        }

        private void btnOpenLogin_Click(object sender, EventArgs e)
        {
            var loginForm = Dependency.GetInstanceOf<LoginForm>();
            loginForm.Show();
            this.Hide();
        }

        private void btnOpenCandidate_Click(object sender, EventArgs e)
        {
            var allJobsForm = Dependency.GetInstanceOf<AllJobsForm>();
            allJobsForm.Show();
            this.Hide();
        }
    }
}
