﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mafuba.App.Forms.Lenguage
{
    public partial class LenguageForm : LenguageFormBase
    {
        public LenguageForm() : base()
        {
            InitializeComponent();
            RegisterTableColumns(new GridColumn
            {
                ColumnDataName = "Name",
                ColumnName = "Nombre"
            }, new GridColumn
            {
                ColumnDataName = "State",
                ColumnName = "Estado"
            });
        }

        private void LenguageForm_Load(object sender, EventArgs e)
        {
            BuildTable();
        }
    }
}
