﻿using Mafuba.BL.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mafuba.App
{
    public partial class LoginForm : Form
    {
        private readonly ILoginService _loginService;
        private readonly AdministrationHomeForm _homeForm;
        public LoginForm(ILoginService loginService, AdministrationHomeForm homeForm)
        {
            _loginService = loginService;
            _homeForm = homeForm;
            InitializeComponent();
        }

        private async void btnLogIn_Click(object sender, EventArgs e)
        {
            this.progressBarLogin.Style = ProgressBarStyle.Marquee;
            this.progressBarLogin.Visible = true;


            this.btnLogIn.Enabled = false;

            await Task.Run(() => LogInUser());
            if (Program.LoggedUser == null)
            {
                //User not found, try again
                this.lblErrorLogin.Show();
            }
            else
            {
                this.Hide();
                _homeForm.Show();
            }
            this.btnLogIn.Enabled = true;
            this.progressBarLogin.Visible = false;


        }

        private void LogInUser()
        {
            var userName = this.txtUser.Text;
            var password = this.txtPassword.Text;

            var user = _loginService.LogIn(userName, password);
            if (user != null)
                Program.LoggedUser = user;
               
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                this.btnLogIn.PerformClick();
            }
        }
    }
}
