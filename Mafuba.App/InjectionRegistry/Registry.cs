﻿using Mafuba.BL.Interfaces;
using Mafuba.BL.Repositories;
using Mafuba.BL.Services;
using Mafuba.Datamodel.Context;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mafuba.App.InjectionRegistry
{
    public static class Registry
    {
        public static void RegisterOnContainer(Container container)
        {
            container.Register<DbContext, MafubaContext>();
            container.Register<ILoginService, LoginService>();
            container.Register(typeof(IRepository<>), typeof(BaseRepository<>));
        }
    }
}
