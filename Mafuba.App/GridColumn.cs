﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mafuba.App
{
    public class GridColumn
    {
        public string ColumnName { get; set; }
        public string ColumnDataName { get; set; }
    }
}
