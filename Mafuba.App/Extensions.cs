﻿using AutoMapper;
using Mafuba.App.AutoMapper;
using Mafuba.App.InjectionRegistry;
using Mafuba.BL.Interfaces;
using Mafuba.BL.Repositories;
using Mafuba.BL.Services;
using Mafuba.Datamodel.Context;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mafuba.App
{
    public static class Extensions
    {
        public static void ForEach(this DataGridViewColumnCollection cols, Action<DataGridViewColumn> action)
        {
            foreach (DataGridViewColumn col in cols)
            {
                action(col);
            }
        }

        public static void RegisterMafubaInjection(this Container container)
        {
            var autoMapperConfig = new MapperConfiguration(cfg => cfg.AddProfile<MafubaMapperProfile>());
            //
            Registry.RegisterOnContainer(container);

            container.RegisterSingleton(autoMapperConfig);
            container.Register<IMapper>(() => autoMapperConfig.CreateMapper(container.GetInstance));
        }
    }
}
