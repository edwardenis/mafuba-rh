﻿
using Mafuba.BL.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Mafuba.App.Services
{
    public class EmailService
    {
        private string _smtpServer { get; set; }
        private int _port { get; set; }
        private string _userName { get; set; }
        private string _password { get; set; }
        private bool _useSsl { get; set; }
        private MailMessage _mailMessage { get; set; }

        public EmailService(string smtpServer, int port, string userName, string password, bool useSsl = false)
        {
            _mailMessage = new MailMessage();
            _smtpServer = smtpServer;
            _port = port;
            _userName = userName;
            _password = password;
            _useSsl = useSsl;
            From(userName);

        }

        public EmailService From(string from)
        {
            _mailMessage.From = new MailAddress(from);
            return this;
        }

        public EmailService To(string emailAddress)
        {
            if (emailAddress.Contains(";"))
            {
                //email address has semi-colon, try split
                var addressSplit = emailAddress.Split(';');
                for (int i = 0; i < addressSplit.Length; i++)
                {
                    _mailMessage.To.Add(new MailAddress(addressSplit[i].Trim()));
                }
            }
            else
            {
                _mailMessage.To.Add(new MailAddress(emailAddress.Trim()));
            }
            return this;
        }

        public EmailService Subject(string subject)
        {
            _mailMessage.Subject = subject;
            return this;
        }

        public EmailService Body(string body, bool isHtml)
        {
            _mailMessage.IsBodyHtml = isHtml;
            _mailMessage.Body = body;
            return this;
        }

        public EmailService UseTemplate(string filePath, bool isHtml = true, object model = null)
        {
            var template = "";

            using (var reader = new StreamReader(File.OpenRead(filePath)))
            {
                template = reader.ReadToEnd();
            }
            if (model != null)
            {
                template = ReplaceModelInTemplate(template, model);
            }
            return Body(template, isHtml);
        }

        private string ReplaceModelInTemplate(string template, object model)
        {
            var modelType = model.GetType();
            var properties = modelType.GetProperties();
            var sb = new StringBuilder(template);
            foreach(var propertyInfo in properties)
            {
                var propName = propertyInfo.Name;
                var propValue = HelperMethods.GetPropertyValue<object, string>(model, propertyInfo);
                sb.Replace($"{{{propName}}}", propValue);
            }

            return sb.ToString();
        }

        public async Task SendAsync()
        {
            using(var smpt = new SmtpClient())
            {
                smpt.Host = _smtpServer;
                smpt.Port = _port;
                smpt.UseDefaultCredentials = false;
                smpt.Credentials = new NetworkCredential(_userName, _password);
                smpt.EnableSsl = _useSsl;

                await smpt.SendMailAsync(_mailMessage);
            }
        }
    }
}
