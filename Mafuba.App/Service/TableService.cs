﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mafuba.App.Service
{
    public class TableService
    {
        public void BuildTable(DataGridView tableGrid, params GridColumn[] gridColumn)
        {

            var columns = GenerateColumns(gridColumn);

            tableGrid.AutoGenerateColumns = false;
            tableGrid.AllowUserToAddRows = false;
            tableGrid.Columns.AddRange(columns);
            //
            tableGrid.AutoResizeColumns();

            tableGrid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

        }

        public DataGridViewColumn[] GenerateColumns(params GridColumn[] columnsInformation)
        {
            var columns = new List<DataGridViewColumn>();



            foreach (var column in columnsInformation)
            {
                DataGridViewCell cell = new DataGridViewTextBoxCell();
                DataGridViewTextBoxColumn col = new DataGridViewTextBoxColumn()
                {
                    CellTemplate = cell,
                    Name = column.ColumnName,
                    HeaderText = column.ColumnName,
                    DataPropertyName = column.ColumnDataName,
                    Resizable = DataGridViewTriState.True,
                    FillWeight = 100,
                    ReadOnly = true,
                    SortMode = DataGridViewColumnSortMode.NotSortable
                };
                columns.Add(col);
            }

            return columns.ToArray();
        }
    }
}
